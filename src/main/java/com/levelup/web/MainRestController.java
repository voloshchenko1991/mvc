package com.levelup.web;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class MainRestController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getMain() {
        return "/static/es.html";
    }
}
