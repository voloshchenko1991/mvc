package com.levelup.web;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by User on 08.10.2017.
 */
@RestController
public class AJAXController {
    private Integer current = 0;
    private Boolean ground = false;
    @RequestMapping(value = "/ajax", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseBody
    public String getGC(@RequestParam int cs, @RequestParam boolean gc) {
        return "{\"limit\":" + cs + ",\"ground\":" + "\"" + (gc?"on":"off") + "\"," +
                "\"mystatus\":\"ready\",\"limit\":" + cs + ",\"current\":10,\"voltage\":220,\"power\":2.5,"+
                "\"energy\":20.3,\"total\":1005.5}";
    }
}
